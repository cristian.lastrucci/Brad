Brad è un'implementazione dell'algoritmo <b>CYK</b> che permette di stabilire 
l'appartenenza di una stringa ad una grammatica CFG nella forma normale di Chomsky.

Lanciando l'algoritmo è possibile <b>selezionare la grammatica di riferimento</b>, utilizzando
quella disponibile all'interno del progetto o caricandone una nuova mediante un file xml
(di cui è necessario specificare il percorso assoluto), purchè quest'ultimo rispetti 
le regole definite all'interno del file .xsd presente tra le risorse del progetto, per la
descrizione di una grammatica CFG normalizzata secondo la CNF.<br />

Una volta caricata la grammatica è possibile inserire la stringa da analizzare; nel caso
in cui la stringa appartenga alla grammatica caricata, il sistema mostrerà l'<b>albero di
derivazione</b> associato alla stringa inserita, che ne dimostra l'appartenza alla grammatica.<br />

Il sistema supporta anche <b>grammatiche ambigue</b>, mostrando per ogni stringa processata,
TUTTI gli alberi di derivazione associati a quest'ultima.

Di seguito viene riportato un esempio di elaborazione di Brad, che utilizza la grammatica
descritta nella pagina [Wikipedia](https://en.wikipedia.org/wiki/CYK_algorithm) 
relativa all'algoritmo CYK.<br /><br />

<img src="bradExecution.png" alt="Brad Execution Example" />