package clast.brad.main;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.log4j.Logger;

import clast.brad.converter.CfgConverter;
import clast.brad.jaxb.Cfg;
import clast.brad.model.CYK;
import clast.brad.model.ContextFreeGrammar;
import clast.brad.utils.BradConsole;

public class Main {
	
	private static Logger logger;
	
	static {
		logger = Logger.getLogger(Main.class);
	}

	public static void main(String[] args) {
		
		BradConsole.printBradHeader();
		
		ContextFreeGrammar cfg = CfgConverter.convert( loadContextFreeGrammar( BradConsole.choiceCfgXMLSourceFile() ) );
		
		BradConsole.printCfg(cfg);
		
		CYK cyk = new CYK(cfg);

		String input = BradConsole.readStringToParse();
		
		if( cyk.parseString(input, " ") ) {
			
			BradConsole.printSuccess(input);
			BradConsole.printParseTrees( cyk );
			
		}else {
			BradConsole.printFailure(input);
		}
		
		BradConsole.printBradFooter();
	}

	private static Cfg loadContextFreeGrammar(String cfgPath) {
		try {
			JAXBContext context = JAXBContext.newInstance(Cfg.class);
			Unmarshaller um = context.createUnmarshaller();

			File cfg = new File( cfgPath );
			
			return (Cfg) um.unmarshal( new FileReader(cfg) );
			
		}catch (NullPointerException npe) {
			logger.error( "Error during Context-free Grammar loading: XML source file not found. " + npe );
		}catch (FileNotFoundException fnfe) {
			logger.error( "Error during Context-free Grammar loading: XML source file not found. " + fnfe );
		}catch (JAXBException e) {
			logger.error( "Error during Context-free Grammar loading: XML source file does not respect expected schema." + e );
		}
		
		return null;
	}

}
