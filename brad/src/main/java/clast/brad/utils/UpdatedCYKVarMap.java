package clast.brad.utils;

import java.util.List;
import java.util.Map;

import clast.brad.model.CYKVariable;

public class UpdatedCYKVarMap {

	private Map<Integer, CYKVariable> map;
	
	private List<Integer> newVerticalRowsPositions;

	public Map<Integer, CYKVariable> getMap() {
		return map;
	}

	public void setMap(Map<Integer, CYKVariable> map) {
		this.map = map;
	}

	public List<Integer> getNewVerticalRowsPositions() {
		return newVerticalRowsPositions;
	}

	public void setNewVerticalRowsPositions(List<Integer> newVerticalRowsPositions) {
		this.newVerticalRowsPositions = newVerticalRowsPositions;
	}	
	
}
