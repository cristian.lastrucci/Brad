package clast.brad.utils;

import java.util.HashMap;
import java.util.Map;

public class ReversedMap<X,Y> {
	
	public Map<Y,X> reverse(Map<X,Y> map){
		
		Map<Y,X> result = new HashMap<Y,X>();
		
		for(Map.Entry<X,Y> entry : map.entrySet()) {
			result.put(entry.getValue(), entry.getKey());
		}
		
		return result;
	}

}
