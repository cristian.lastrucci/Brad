package clast.brad.utils;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

import org.apache.log4j.Logger;

import clast.brad.model.CYK;
import clast.brad.model.CYKVariable;
import clast.brad.model.ContextFreeGrammar;
import clast.brad.model.ProductionRule;

public class BradConsole {
	
	private static Logger logger;
	private static Scanner scanner;
	
	static {
		logger = Logger.getLogger(BradConsole.class);		
		scanner = new Scanner(System.in);
	}
	
	public static void printBradHeader() {
		logger.info("-----------------------------------------------------------------------------------------------------------------------------------------------------");
		logger.info("-------------------------------------------------------------------- Brad v1.0 ----------------------------------------------------------------------");
		logger.info("-----------------------------------------------------------------------------------------------------------------------------------------------------");
		logger.info("");
	}

	public static String choiceCfgXMLSourceFile() {
		logger.info("Load Context-free grammar:");
		logger.info("   1: Using default CFG");
		logger.info("   2: Inserting absolute path of CFG xml source file");
		
		String cfgPath = null;
		int cfgLoadingType = scanner.nextInt();

		if( cfgLoadingType == 1 ) {
			cfgPath = new File("src/main/resources/cfg.xml").getAbsolutePath();
		}else if(cfgLoadingType == 2 ) {
			logger.info("   Absolute xml path: ");
			scanner = new Scanner(System.in);
			cfgPath = scanner.nextLine();
		}
		
		return cfgPath;
	}
	
	public static void printCfg(ContextFreeGrammar cfg) {
		
		String variables = "   V = { ";
		for( String var : cfg.getVariables() ) {
			variables = variables + var + ", ";
		}
		variables = variables.substring(0, variables.lastIndexOf(',')) + " }";
		
		String ts = "   T = { ";
		for( String t : cfg.getTerminalSymbols() ) {
			ts = ts + t + ", ";
		}
		ts = ts.substring(0, ts.lastIndexOf(',')) + " }";
		
		String initialSymbol = "   S = " + cfg.getInitialSymbol();
		
		String productionRules = "   P = { ";
		for( ProductionRule pr : cfg.getProductionRules() ) {
			String productions = "";
			for( String p : pr.getProduction() ) {
				productions = productions + p + "-";
			}
			productions = productions.substring(0, productions.lastIndexOf('-'));
			productionRules = productionRules + pr.getVariable() + " -> " + productions + ", ";
		}
		productionRules = productionRules.substring(0, productionRules.lastIndexOf(',')) + " }";
		
		
		logger.info("Cfg:");
		logger.info(variables);
		logger.info(ts);
		logger.info(initialSymbol);
		logger.info(productionRules);
		logger.info("");
	}
	
	public static String readStringToParse() {
		logger.info("Insert string to parse:");
		scanner = new Scanner(System.in);
		return scanner.nextLine();
	}

	public static void printSuccess(String input) {
		logger.info("STRING \"" + input + "\" BELONGS TO CFG.");
	}
	
	public static void printFailure(String input) {
		logger.info("STRING \"" + input + "\" NOT BELONGS TO CFG.");
	}
	
	public static void printParseTrees(CYK cyk) {
		Set<CYKVariable> rootsVars = cyk.findRootCYKVariables();
		for(CYKVariable root : rootsVars) {
			
			Set<CYKVariable> treeVars = new HashSet<CYKVariable>();
			treeVars.add(root);
			
			List<CYKVariable> unexploredTreeVars = new ArrayList<CYKVariable>();
			unexploredTreeVars.add(root);
			
			while( !unexploredTreeVars.isEmpty() ) {
				CYKVariable var = unexploredTreeVars.remove(0);
				if( !var.getParents().isEmpty() ) {
					treeVars.addAll(var.getParents());
					unexploredTreeVars.addAll(var.getParents());
				}
			}
			
			printParseTree(cyk.getSymbols(),treeVars);
		}
	}

	private static void printParseTree(List<String> symbols, Set<CYKVariable> treeVars) {
		logger.info("");
		logger.info("Parse tree:");
		logger.info("");
		
		String terminalSymbolsRow = "";
		Map<Integer,Integer> terminalSymbolsMap = new HashMap<Integer,Integer>();
		int maxPos = 0;
		int i = 0;
		for(String ts : symbols) {
			maxPos = terminalSymbolsRow.length() + ((ts.length()/2)+1);
			terminalSymbolsMap.put(i, maxPos);
			terminalSymbolsRow = terminalSymbolsRow + ts + " ";
			i++;
		}
		
		logger.info(terminalSymbolsRow);
		
		logger.info( printVerticalRowsLine(terminalSymbolsMap.values(), maxPos) );
		
		Map<Integer, CYKVariable> cykVariablesMap = assignTerminalCYKVariablesPosition(terminalSymbolsMap, CYK.findCYKVariables(treeVars, 1));
		
		List<Integer> newVerticalRowsPositions = new ArrayList<Integer>(terminalSymbolsMap.values());
		
		for(i=2; i<=symbols.size()+1; i++) {
			
			logger.info( printCYKVariablesLine(cykVariablesMap,newVerticalRowsPositions) );
			
			Set<CYKVariable> nextLevelVariables = CYK.findCYKVariables(treeVars, i);
			
			if(i<=symbols.size()) {
			
				Map<CYKVariable,List<Integer>> orizontalBoundaries = findOrizontalRowsBoundaries( cykVariablesMap,nextLevelVariables );
				
				logger.info( printInternalLevelConnections(cykVariablesMap.keySet(), Collections.max(cykVariablesMap.keySet()), orizontalBoundaries) );
				
				UpdatedCYKVarMap updatedCYKVarMap = updateCYKVariablesMap(cykVariablesMap, orizontalBoundaries, newVerticalRowsPositions);
				cykVariablesMap = updatedCYKVarMap.getMap();
				newVerticalRowsPositions = updatedCYKVarMap.getNewVerticalRowsPositions();
				
				logger.info( printVerticalRowsLine(cykVariablesMap.keySet(), Collections.max(cykVariablesMap.keySet())));
			}
		}
		
		logger.info("");
	}

	private static String printVerticalRowsLine(Collection<Integer> positions, int maxPos) {
		String row = "";
		for(int i=1; i<=maxPos; i++) {
			if(positions.contains(i)) {
				row = row + "|";
			}else {
				row = row + " ";
			}
		}
		return row;
	}
	
	private static Map<Integer, CYKVariable> assignTerminalCYKVariablesPosition(Map<Integer, Integer> terminalSymbolsMap, Set<CYKVariable> vars) {
		Map<Integer, CYKVariable> result = new HashMap<Integer, CYKVariable>();
		for(CYKVariable var : vars) {
			if(terminalSymbolsMap.containsKey(var.getSymbolIndex())) {
				result.put(terminalSymbolsMap.get(var.getSymbolIndex()),var);
			}
		}
		for(Integer pos : terminalSymbolsMap.values()) {
			if( !result.containsKey(pos) ) {
				result.put(pos, null);
			}
		}
		return result;
	}
	
	private static String printCYKVariablesLine(Map<Integer, CYKVariable> cykVariablesMap, List<Integer> newVerticalRowsPositions) {
		
		String row = "";
		for(int i=1; i<=Collections.max( cykVariablesMap.keySet() ); i++) {
			if(cykVariablesMap.keySet().contains(i)) {
				if(cykVariablesMap.get(i) == null || !newVerticalRowsPositions.contains(i) ) {
					row = row + "|";
				}else {
					row = row + cykVariablesMap.get(i).getVariable();
					i = i + cykVariablesMap.get(i).getVariable().length() - 1;
				}
			}else {
				row = row + " ";
			}
		}
		return row;
	}
	
	private static Map<CYKVariable,List<Integer>> findOrizontalRowsBoundaries(Map<Integer, CYKVariable> currentLevelVarsMap, Set<CYKVariable> nextLevelVars) {
		
		Map<CYKVariable,List<Integer>> result = new HashMap<CYKVariable,List<Integer>>();
		
		Map<CYKVariable,Integer> reverseMap = new ReversedMap<Integer,CYKVariable>().reverse(currentLevelVarsMap);
		
		for(CYKVariable v : nextLevelVars) {
			if( !v.getParents().isEmpty() && v.getParents().size() == 2 && currentLevelVarsMap.values().containsAll(v.getParents())) {
				int orizontalRowStartPos = reverseMap.get( v.getParents().get(0) );
				int orizontalRowLenght = reverseMap.get( v.getParents().get(1) ) - orizontalRowStartPos;
				result.put(v,Arrays.asList(orizontalRowStartPos,orizontalRowLenght));
			}
		}
		return result;
	}
	
	private static String printInternalLevelConnections(Set<Integer> verticalRowsPositions, int maxPos, Map<CYKVariable,List<Integer>> orizontalRowsBoundaries) {
		
		String row = "";
		for(int i=1; i<=maxPos; i++) {
			if(verticalRowsPositions.contains(i)) {
				row = row + "|";
			}else if( isAnArchPosition(i,orizontalRowsBoundaries) ){
				row = row + "_";
			}else {
				row = row + " ";				
			}
		}
		return row;
	}
	
	private static boolean isAnArchPosition( int pos, Map<CYKVariable,List<Integer>> orizontalRowsBoundaries ) {
		
		Map<Integer,Integer> boundariesMap = new HashMap<Integer, Integer>();
		for(Map.Entry<CYKVariable,List<Integer>> entry : orizontalRowsBoundaries.entrySet()) {
			boundariesMap.put(entry.getValue().get(0), entry.getValue().get(1));
		}
		
		for(Map.Entry<Integer,Integer> entry : boundariesMap.entrySet()) {
			if( pos>entry.getKey() && pos<(entry.getKey()+entry.getValue()) ) {
				return true;
			}
		}
		return false;
	}
	
	private static UpdatedCYKVarMap updateCYKVariablesMap(Map<Integer, CYKVariable> currentLevelVarsMap, 	Map<CYKVariable, List<Integer>> orizontalRowsBoundaries, List<Integer> newVerticalRowsPositions) {
		
		Map<CYKVariable,Integer> reversedCurrentLevelVarsMap = new ReversedMap<Integer, CYKVariable>().reverse(currentLevelVarsMap);
		newVerticalRowsPositions = new ArrayList<Integer>();
		
		for(Map.Entry<CYKVariable,List<Integer>> entry : orizontalRowsBoundaries.entrySet()) {
			if( !entry.getKey().getParents().isEmpty() && 
					entry.getKey().getParents().size() == 2 && 
							reversedCurrentLevelVarsMap.keySet().containsAll(entry.getKey().getParents()) && 
								!reversedCurrentLevelVarsMap.keySet().contains(entry.getKey())) {
				for(CYKVariable var : entry.getKey().getParents()) {
					reversedCurrentLevelVarsMap.remove(var);
				}
				Integer newVerticalRowPos = (((entry.getValue().get(0)*2)+entry.getValue().get(1))/2);
				reversedCurrentLevelVarsMap.put(entry.getKey(), newVerticalRowPos);
				newVerticalRowsPositions.add(newVerticalRowPos);
			}
		}
		
		UpdatedCYKVarMap result = new UpdatedCYKVarMap();
		result.setMap( new ReversedMap<CYKVariable,Integer>().reverse(reversedCurrentLevelVarsMap) );
		result.setNewVerticalRowsPositions(newVerticalRowsPositions);
		
		return result;
	}

	public static void printBradFooter() {
		logger.info("");
		logger.info("-----------------------------------------------------------------------------------------------------------------------------------------------------");
		logger.info("-----------------------------------------------------------------------------------------------------------------------------------------------------");
	}

}
