package clast.brad.model;

import java.util.ArrayList;
import java.util.List;

public class CYKVariable {
	
	private int substringLenght;
	
	private int symbolIndex;
	
	private String variable;
	
	private List<CYKVariable> parents;
	
	public CYKVariable() {
		parents = new ArrayList<CYKVariable>();
	}

	public int getSubstringLenght() {
		return substringLenght;
	}

	public void setSubstringLenght(int substringLenght) {
		this.substringLenght = substringLenght;
	}

	public int getSymbolIndex() {
		return symbolIndex;
	}

	public void setSymbolIndex(int symbolIndex) {
		this.symbolIndex = symbolIndex;
	}
	
	public String getVariable() {
		return variable;
	}
	
	public void setVariable(String variable) {
		this.variable = variable;
	}

	public List<CYKVariable> getParents() {
		return parents;
	}

	public void setParents(List<CYKVariable> parents) {
		this.parents = parents;
	}

}
