package clast.brad.model;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class ContextFreeGrammar {

	Set<String> variables;

	Set<String> terminalSymbols;

	String initialSymbol;

	Set<ProductionRule> productionRules;

	public Set<String> getVariables() {
		return variables;
	}

	public void setVariables(Set<String> variables) {
		this.variables = variables;
	}

	public Set<String> getTerminalSymbols() {
		return terminalSymbols;
	}

	public void setTerminalSymbols(Set<String> terminalSymbols) {
		this.terminalSymbols = terminalSymbols;
	}

	public String getInitialSymbol() {
		return initialSymbol;
	}

	public void setInitialSymbol(String initialSymbol) {
		this.initialSymbol = initialSymbol;
	}

	public Set<ProductionRule> getProductionRules() {
		return productionRules;
	}

	public void setProductionRules(Set<ProductionRule> productionRules) {
		this.productionRules = productionRules;
	}
	
	public Set<ProductionRule> findTerminalProductionRules(String terminalSymbol) {
		Set<ProductionRule> result = new HashSet<ProductionRule>();
		for(ProductionRule pr : productionRules) {
			if(ProductionRuleType.TERMINAL.equals(pr.getProductionType()) && terminalSymbol.equals(pr.getProduction().get(0)) ) {
				result.add(pr);
			}
		}
		return result;
	}

	public Set<ProductionRule> findBinaryProductionRules(String firstVar, String secondVar) {
		Set<ProductionRule> result = new HashSet<ProductionRule>();
		for(ProductionRule pr : productionRules) {
			if(ProductionRuleType.BINARY.equals(pr.getProductionType()) && 
				firstVar.equals(pr.getProduction().get(0)) && 
				secondVar.equals(pr.getProduction().get(1)) ) {
				result.add(pr);
			}
		}
		return result;
	}

}
