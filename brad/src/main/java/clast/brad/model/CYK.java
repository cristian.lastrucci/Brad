package clast.brad.model;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class CYK {
	
	private ContextFreeGrammar cfg;
	
	private List<String> symbols;
	private int stringLenght;
	
	private Set<CYKVariable> cykVariables;
	
	public CYK( ContextFreeGrammar cfg) {
		this.cfg = cfg;
	}
	
	public List<String> getSymbols() {
		return Collections.unmodifiableList(symbols);
	}

	public Set<CYKVariable> getCykVariables() {
		return Collections.unmodifiableSet(cykVariables);
	}

	public boolean parseString(String input, String separator) {
		
		initCYK(input, separator);
		
		for(int i=0; i<stringLenght; i++) {
			for(int j=0; j<stringLenght-i; j++) {
				if( i == 0) {
					findTerminalCYKVariables(j);
				}else {
					findBinaryCYKVariables(i+1,j);
				}
			}
		}
		
		return cykExecutionOutcome();
	}

	private void initCYK(String input, String separator) {
		symbols = Arrays.asList( input.split(separator) );
		stringLenght = symbols.size();
		cykVariables = new HashSet<CYKVariable>();
	}

	private void findTerminalCYKVariables(int j) {
		for( ProductionRule pr : cfg.findTerminalProductionRules(symbols.get(j)) ) {
			CYKVariable var = new CYKVariable();
			var.setSubstringLenght(1);
			var.setSymbolIndex(j);
			var.setVariable(pr.getVariable());
			cykVariables.add(var);
		}
	}
	
	private void findBinaryCYKVariables(int stringLenght, int symbolIndex) {
		for(int i=stringLenght-1, j=1; i>0; i--, j++) {
			
			Set<CYKVariable> firstSubstringVars = findCYKVariables(symbolIndex,i);
			Set<CYKVariable> secondSubstringVars = findCYKVariables(symbolIndex+i,j);
			
			if( !firstSubstringVars.isEmpty() && !secondSubstringVars.isEmpty() ) {
				
				findBinaryCYKVariables(stringLenght, symbolIndex, firstSubstringVars, secondSubstringVars);
			}
		}
	}

	private Set<CYKVariable> findCYKVariables(int substringStartIndex, int substringLenght) {
		Set<CYKVariable> result = new HashSet<CYKVariable>();
		for(CYKVariable var : cykVariables) {
			if(var.getSymbolIndex() == substringStartIndex && var.getSubstringLenght() == substringLenght ) {
				result.add(var);
			}
		}
		return result;
	}
	
	private void findBinaryCYKVariables(int stringLenght, int symbolIndex, Set<CYKVariable> firstSubstringVars, Set<CYKVariable> secondSubstringVars) {
		for(CYKVariable v1 : firstSubstringVars) {
			for(CYKVariable v2 : secondSubstringVars) {
				for(ProductionRule pr : cfg.findBinaryProductionRules(v1.getVariable(), v2.getVariable()) ) {
					
					CYKVariable var = new CYKVariable();
					var.setSubstringLenght(stringLenght);
					var.setSymbolIndex(symbolIndex);
					var.setVariable(pr.getVariable());
					var.setParents( Arrays.asList(v1, v2) );
					
					cykVariables.add(var);
				}
			}
		}
	}
	
	private boolean cykExecutionOutcome() {
		return !findRootCYKVariables().isEmpty();
	}
	
	public Set<CYKVariable> findRootCYKVariables() {
		Set<CYKVariable> result = new HashSet<CYKVariable>();
		for(CYKVariable var : cykVariables) {
			if(var.getSubstringLenght() == stringLenght &&
				var.getSymbolIndex() == 0 &&
				var.getVariable().equals( cfg.getInitialSymbol()) ) {
				result.add(var);
			}
		}
		return result;
	}
	
	public static Set<CYKVariable> findCYKVariables( Set<CYKVariable> tree, int substringLenght ) {
		Set<CYKVariable> result = new HashSet<CYKVariable>();
		for(CYKVariable var : tree) {
			if(var.getSubstringLenght() == substringLenght ) {
				result.add(var);
			}
		}
		return result;
	}

}
