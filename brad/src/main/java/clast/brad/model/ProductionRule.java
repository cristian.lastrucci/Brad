package clast.brad.model;

import java.util.List;

public class ProductionRule {

	private ProductionRuleType productionType;

	private String variable;

	private List<String> production;

	public ProductionRuleType getProductionType() {
		return productionType;
	}

	public void setProductionType(ProductionRuleType productionType) {
		this.productionType = productionType;
	}

	public String getVariable() {
		return variable;
	}

	public void setVariable(String variable) {
		this.variable = variable;
	}

	public List<String> getProduction() {
		return production;
	}

	public void setProduction(List<String> production) {
		this.production = production;
	}

}
