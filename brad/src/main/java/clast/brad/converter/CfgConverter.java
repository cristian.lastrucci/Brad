package clast.brad.converter;

import java.util.HashSet;
import java.util.Set;

import clast.brad.jaxb.Cfg;
import clast.brad.model.ContextFreeGrammar;
import clast.brad.model.ProductionRule;
import clast.brad.model.ProductionRuleType;

public class CfgConverter {

	public static ContextFreeGrammar convert(Cfg cfg) {
		
		ContextFreeGrammar result = new ContextFreeGrammar();
		
		result.setVariables( new HashSet<String>(cfg.getVariables().getVariable()) );
		result.setTerminalSymbols( new HashSet<String>(cfg.getTerminalSymbols().getTerminalSymbol()) );
		result.setInitialSymbol(cfg.getInitialSymbol());
		result.setProductionRules( convertProductionRules(cfg) );
		
		validateCfg(result);
		
		return result;
	}

	private static Set<ProductionRule> convertProductionRules(Cfg cfg) {
		
		Set<ProductionRule> rules = new HashSet<ProductionRule>();
		
		for(clast.brad.jaxb.ProductionRule pr : cfg.getProductionRules().getProductionRule()) {
			rules.add( convert(pr) );
		}
		
		return rules;
	}
	
	private static ProductionRule convert(clast.brad.jaxb.ProductionRule pr) {
		
		ProductionRule result = new ProductionRule();
		
		if(pr.getProduction() == null || pr.getProduction().getSymbol() == null) {
			throw new IllegalArgumentException("Cfg convertion error: Production rule must has at least one production.");			
		}
		
		if(pr.getProduction().getSymbol().isEmpty()) {
			throw new IllegalArgumentException("Cfg convertion error: Production rule must has at least one production.");
		}else if(pr.getProduction().getSymbol().size() == 1) {
			result.setProductionType(ProductionRuleType.TERMINAL);
		}else if(pr.getProduction().getSymbol().size() == 2) {
			result.setProductionType(ProductionRuleType.BINARY);
		}else {
			throw new IllegalArgumentException("Cfg convertion error: Production rule must has at most two production.");
		}
		
		result.setProduction( pr.getProduction().getSymbol() );
		
		if(pr.getVariable() == null) {
			throw new IllegalArgumentException("Cfg convertion error: Variable of production rule not specified.");			
		}
		
		result.setVariable( pr.getVariable() );
		
		return result;
	}
	
	private static void validateCfg(ContextFreeGrammar result) {
		
		for(String var : result.getVariables() ) {
			if( result.getTerminalSymbols().contains(var)) {
				throw new IllegalArgumentException("Cfg convertion error: Variables and Terminal symbols of Cfg must be disjointed sets.");
			}
		}
		
		for(String ts : result.getTerminalSymbols() ) {
			if( result.getVariables().contains(ts)) {
				throw new IllegalArgumentException("Cfg convertion error: Variables and Terminal symbols of Cfg must be disjointed sets.");
			}
		}
		
		if( !result.getVariables().contains(result.getInitialSymbol()) ) {
			throw new IllegalArgumentException("Cfg convertion error: The initial symbol of Cfg must belong to the variables set of Cfg.");
		}
		
		for( ProductionRule pr : result.getProductionRules() ) {
			for( String p : pr.getProduction() ) {
				if( !result.getTerminalSymbols().contains(p) ) {
					boolean twoVarsProductionCheck = true;
					for(String s : p.split(" ")) {
						if( !result.getVariables().contains(s) ) {
							twoVarsProductionCheck = false;
						}
					}
					if( !twoVarsProductionCheck ) {
						throw new IllegalArgumentException("Cfg convertion error: Every production must be to a terminal symbols or a couple of variables of Cgf.");
					}
				}
			}
			if( !result.getVariables().contains(pr.getVariable()) ) {
				throw new IllegalArgumentException("Cfg convertion error: The variable of every production must belong to the varibles set of Cgf.");
			}
		}
	}
	
}
