//
// Questo file è stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andrà persa durante la ricompilazione dello schema di origine. 
// Generato il: 2018.03.28 alle 06:23:56 PM CEST 
//


package clast.brad.jaxb;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per productionRule complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="productionRule">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="variable" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="production" type="{http://www.example.org/contextFreeGrammar}production"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "productionRule", propOrder = {
    "variable",
    "production"
})
public class ProductionRule {

    @XmlElement(required = true)
    protected String variable;
    @XmlElement(required = true)
    protected Production production;

    /**
     * Recupera il valore della proprietà variable.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVariable() {
        return variable;
    }

    /**
     * Imposta il valore della proprietà variable.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVariable(String value) {
        this.variable = value;
    }

    /**
     * Recupera il valore della proprietà production.
     * 
     * @return
     *     possible object is
     *     {@link Production }
     *     
     */
    public Production getProduction() {
        return production;
    }

    /**
     * Imposta il valore della proprietà production.
     * 
     * @param value
     *     allowed object is
     *     {@link Production }
     *     
     */
    public void setProduction(Production value) {
        this.production = value;
    }

}
