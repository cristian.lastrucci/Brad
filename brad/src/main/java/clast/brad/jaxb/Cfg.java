//
// Questo file è stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andrà persa durante la ricompilazione dello schema di origine. 
// Generato il: 2018.03.28 alle 06:23:56 PM CEST 
//


package clast.brad.jaxb;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per anonymous complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="variables" type="{http://www.example.org/contextFreeGrammar}variables"/>
 *         &lt;element name="terminalSymbols" type="{http://www.example.org/contextFreeGrammar}terminalSymbols"/>
 *         &lt;element name="initialSymbol" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="productionRules" type="{http://www.example.org/contextFreeGrammar}productionRules"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "variables",
    "terminalSymbols",
    "initialSymbol",
    "productionRules"
})
@XmlRootElement(name = "cfg")
public class Cfg {

    @XmlElement(required = true)
    protected Variables variables;
    @XmlElement(required = true)
    protected TerminalSymbols terminalSymbols;
    @XmlElement(required = true)
    protected String initialSymbol;
    @XmlElement(required = true)
    protected ProductionRules productionRules;

    /**
     * Recupera il valore della proprietà variables.
     * 
     * @return
     *     possible object is
     *     {@link Variables }
     *     
     */
    public Variables getVariables() {
        return variables;
    }

    /**
     * Imposta il valore della proprietà variables.
     * 
     * @param value
     *     allowed object is
     *     {@link Variables }
     *     
     */
    public void setVariables(Variables value) {
        this.variables = value;
    }

    /**
     * Recupera il valore della proprietà terminalSymbols.
     * 
     * @return
     *     possible object is
     *     {@link TerminalSymbols }
     *     
     */
    public TerminalSymbols getTerminalSymbols() {
        return terminalSymbols;
    }

    /**
     * Imposta il valore della proprietà terminalSymbols.
     * 
     * @param value
     *     allowed object is
     *     {@link TerminalSymbols }
     *     
     */
    public void setTerminalSymbols(TerminalSymbols value) {
        this.terminalSymbols = value;
    }

    /**
     * Recupera il valore della proprietà initialSymbol.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInitialSymbol() {
        return initialSymbol;
    }

    /**
     * Imposta il valore della proprietà initialSymbol.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInitialSymbol(String value) {
        this.initialSymbol = value;
    }

    /**
     * Recupera il valore della proprietà productionRules.
     * 
     * @return
     *     possible object is
     *     {@link ProductionRules }
     *     
     */
    public ProductionRules getProductionRules() {
        return productionRules;
    }

    /**
     * Imposta il valore della proprietà productionRules.
     * 
     * @param value
     *     allowed object is
     *     {@link ProductionRules }
     *     
     */
    public void setProductionRules(ProductionRules value) {
        this.productionRules = value;
    }

}
